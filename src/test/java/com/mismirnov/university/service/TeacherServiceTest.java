package com.mismirnov.university.service;

import com.mismirnov.university.domain.Course;
import com.mismirnov.university.domain.Teacher;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class TeacherServiceTest {

    private TeacherService service;

    @Before
    public void setUp() {
        service = new TeacherService();
    }

    @Test
    public void givenTeacher_whenAddTeacher_thenTeacherAdded() {
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);

        assertThat(service.getAll(), hasItem(teacher));
    }

    @Test
    public void givenExistingTeacher_whenAddTeacher_thenNoChanges() {
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);
        int expectedSize = service.getAll().size();

        service.add(new Teacher("abc", "cde", "f"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenExistingTeacher_whenRemoveTeacher_thenTeacherRemoved() {
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);
        service.remove(teacher);

        assertThat(service.getAll(), not(hasItem(teacher)));
    }

    @Test
    public void givenNewTeacher_whenRemoveTeacher_thenNoChanges() {
        int expectedSize = service.getAll().size();
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.remove(teacher);

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenNewCourse_whenAddTeacherCourse_thenAddTeacherCourse() {
        Course course = new Course("abc", "bca");
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);

        service.addCourse(teacher, course);

        assertThat(service.getAll().get(0).getCourses(), hasItem(course));
    }

    @Test
    public void givenExistingCourse_whenAddTeacherCourse_thenNoChanges() {
        Course course = new Course("abc", "bca");
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);
        service.addCourse(teacher, course);
        int expectedSize = service.getAll().get(0).getCourses().size();

        service.addCourse(teacher, new Course("abc", "bca"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }


    @Test
    public void givenExistingCourse_whenRemoveTeacherCourse_thenRemoveTeacherCourse() {
        Course course = new Course("abc", "bca");
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);
        service.addCourse(teacher, course);

        service.removeCourse(teacher, course);

        assertThat(service.getAll().get(0).getCourses(), not(hasItem(course)));
    }

    @Test
    public void givenNewCourse_whenRemoveTeacherCourse_thenNoChanges() {
        Course course = new Course("abc", "bca");
        Teacher teacher = new Teacher("abc", "cde", "f");
        service.add(teacher);
        service.addCourse(teacher, course);
        int expectedSize = service.getAll().get(0).getCourses().size();

        service.removeCourse(teacher, new Course("cda", "bac"));

        assertThat(service.getAll().get(0).getCourses(), hasSize(expectedSize));
    }
}
