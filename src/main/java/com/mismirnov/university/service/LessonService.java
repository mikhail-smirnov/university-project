package com.mismirnov.university.service;

import com.mismirnov.university.domain.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class LessonService {

    private List<Lesson> lessons = new ArrayList<>();

    public List<Lesson> getAll() {
        return lessons;
    }

    public void add(Lesson lesson) {
        if (!lessons.contains(lesson) && isLessonPossible(lesson)) {
            lessons.add(lesson);
        }
    }

    public void remove(Lesson lesson) {
        if (lessons.contains(lesson)) {
            lessons.remove(lesson);
        }
    }

    public void update(Lesson originalLesson, Lesson updatedLesson) {
        int lessonIndex = lessons.indexOf(originalLesson);
        if (isUpdatePossible(originalLesson, updatedLesson)) {
            Lesson lesson = lessons.get(lessonIndex);
            lesson.setCourse(updatedLesson.getCourse());
            lesson.setTeacher(updatedLesson.getTeacher());
            lesson.setDate(updatedLesson.getDate());
            lesson.setLessonTime(updatedLesson.getLessonTime());
            lesson.setClassRoom(updatedLesson.getClassRoom());
            lesson.setGroups(updatedLesson.getGroups());
        }
    }

    public void addGroup(Lesson lesson, Group group) {
        int lessonIndex = lessons.indexOf(lesson);
        if (lessonIndex != -1) {
            int groupIndex = lessons.get(lessonIndex).getGroups().indexOf(group);
            if (groupIndex == -1) {
                lessons.get(lessonIndex).getGroups().add(group);
            }
        }
    }

    public void removeGroup(Lesson lesson, Group group) {
        int lessonIndex = lessons.indexOf(lesson);
        if (lessonIndex != -1) {
            int groupIndex = lessons.get(lessonIndex).getGroups().indexOf(group);
            if (groupIndex != -1) {
                lessons.get(lessonIndex).getGroups().remove(groupIndex);
            }
        }
    }

    public List<Lesson> getTeacherSchedule(Teacher teacher, LocalDate start, LocalDate end) {
        List<Lesson> schedule = new ArrayList<>();
        for (Lesson lesson : lessons) {
            if (lesson.getDate().isAfter(start.minusDays(1)) && lesson.getDate().isBefore(end.plusDays(1))
                    && lesson.getTeacher().equals(teacher)) {
                schedule.add(lesson);
            }
        }
        return schedule;
    }

    public List<Lesson> getStudentSchedule(Student student, LocalDate start, LocalDate end) {
        List<Lesson> schedule = new ArrayList<>();
        for (Lesson lesson : lessons) {
            for (Group group : lesson.getGroups()) {
                for (Student studentInGroup : group.getStudents()) {
                    if (lesson.getDate().isAfter(start.minusDays(1)) && lesson.getDate().isBefore(end.plusDays(1))
                            && studentInGroup.equals(student)) {
                        schedule.add(lesson);
                    }
                }
            }
        }
        return schedule;
    }

    private boolean isUpdatePossible(Lesson originalLesson, Lesson updatedLesson) {
        int lessonIndex = lessons.indexOf(originalLesson);
        if (lessonIndex != -1) {
            Lesson lesson = lessons.get(lessonIndex);
            lessons.remove(lessonIndex);
            boolean answer = isLessonPossible(updatedLesson);
            lessons.add(lesson);
            return answer;
        }
        return false;
    }

    private boolean isLessonPossible(Lesson lesson) {
        return isTeacherFree(lesson.getTeacher(), lesson.getDate(), lesson.getLessonTime()) &&
                isClassRoomFree(lesson.getClassRoom(), lesson.getDate(), lesson.getLessonTime()) &&
                isGroupsFree(lesson.getGroups(), lesson.getDate(), lesson.getLessonTime());
    }


    private boolean isTeacherFree(Teacher teacher, LocalDate date, LessonTime lessonTime) {
        for (Lesson lesson : lessons) {
            if (lesson.getTeacher().equals(teacher) &&
                    lesson.getDate().equals(date) &&
                    lesson.getLessonTime().equals(lessonTime)) {
                return false;
            }
        }
        return true;
    }

    private boolean isClassRoomFree(ClassRoom classRoom, LocalDate date, LessonTime lessonTime) {
        for (Lesson lesson : lessons) {
            return !(lesson.getClassRoom().equals(classRoom) &&
                    lesson.getDate().equals(date) &&
                    lesson.getLessonTime().equals(lessonTime));
        }
        return true;
    }

    private boolean isGroupsFree(List<Group> groups, LocalDate date, LessonTime lessonTime) {
        for (Group group : groups) {
            for (Lesson lesson : lessons) {
                return !(lesson.getGroups().contains(group) &&
                        lesson.getDate().equals(date) &&
                        lesson.getLessonTime().equals(lessonTime));
            }
        }
        return true;
    }
}
