package com.mismirnov.university;

import com.mismirnov.university.domain.*;
import com.mismirnov.university.service.*;

import java.time.LocalDate;
import java.util.List;

public class University {

    private StudentService studentService;
    private TeacherService teacherService;
    private GroupService groupService;
    private ClassRoomService classRoomService;
    private LessonService lessonService;
    private CourseService courseService;
    private LessonTimeService timeService;

    public University(StudentService studentService, TeacherService teacherService, GroupService groupService,
                      ClassRoomService classRoomService, LessonService lessonService, CourseService courseService, LessonTimeService timeService) {
        this.studentService = studentService;
        this.teacherService = teacherService;
        this.groupService = groupService;
        this.classRoomService = classRoomService;
        this.lessonService = lessonService;
        this.courseService = courseService;
        this.timeService = timeService;
    }

    public List<Student> getStudents() {
        return studentService.getAll();
    }

    public List<Teacher> getTeachers() {
        return teacherService.getAll();
    }

    public List<Group> getGroups() {
        return groupService.getAll();
    }

    public List<ClassRoom> getClassRooms() {
        return classRoomService.getAll();
    }

    public List<Lesson> getLessons() {
        return lessonService.getAll();
    }

    public List<Course> getCourses() {
        return courseService.getAll();
    }

    public List<LessonTime> getLessonTimes() {
        return timeService.getAll();
    }

    public void updateLesson(Lesson originalLesson, Lesson updatedLesson) {
        lessonService.update(originalLesson, updatedLesson);
    }

    public void addLessonGroup(Lesson lesson, Group group) {
        lessonService.addGroup(lesson, group);
    }

    public void removeLessonGroup(Lesson lesson, Group group) {
        lessonService.removeGroup(lesson, group);
    }

    public void addTeacher(Teacher teacher) {
        teacherService.add(teacher);
    }

    public void removeTeacher(Teacher teacher) {
        teacherService.remove(teacher);
    }

    public void addTeacherCourse(Teacher teacher, Course course) {
        teacherService.addCourse(teacher, course);
    }

    public void removeTeacherCourse(Teacher teacher, Course course) {
        teacherService.removeCourse(teacher, course);
    }

    public void addGroup(Group group) {
        groupService.add(group);
    }

    public void removeGroup(Group group) {
        groupService.remove(group);
    }

    public Group getStudentGroup(Student student) {
        return groupService.getStudentGroup(student);
    }

    public void addClassRoom(ClassRoom classRoom) {
        classRoomService.add(classRoom);
    }

    public void removeClassRoom(ClassRoom classRoom) {
        classRoomService.remove(classRoom);
    }

    public void addStudent(Student student) {
        studentService.add(student);
    }

    public void addStudentToGroup(Student student, Group group) {
        groupService.addStudent(group, student);
    }

    public void removeStudent(Student student) {
        studentService.remove(student);
        groupService.removeStudent(student);
    }

    public void changeStudentGroup(Student student, Group oldGroup, Group newGroup) {
        groupService.changeStudentGroup(student, oldGroup, newGroup);
    }

    public void addCourse(Course course) {
        courseService.add(course);
    }

    public void removeCourse(Course course) {
        courseService.remove(course);
    }

    public List<Lesson> getTeacherSchedule(Teacher teacher, LocalDate start, LocalDate end) {
        return lessonService.getTeacherSchedule(teacher, start, end);
    }

    public List<Lesson> getStudentSchedule(Student student, LocalDate start, LocalDate end) {
        return lessonService.getStudentSchedule(student, start, end);
    }

    public void addLesson(Lesson lesson) {
        lessonService.add(lesson);
    }

    public void removeLesson(Lesson lesson) {
        lessonService.remove(lesson);
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void setTeacherService(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    public void setGroupService(GroupService groupService) {
        this.groupService = groupService;
    }

    public void setClassRoomService(ClassRoomService classRoomService) {
        this.classRoomService = classRoomService;
    }

    public void setLessonService(LessonService lessonService) {
        this.lessonService = lessonService;
    }

    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }

    public void setTimeService(LessonTimeService timeService) {
        this.timeService = timeService;
    }

}
