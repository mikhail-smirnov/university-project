package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.Course;


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class CourseMenu {


    private Scanner scanner;
    private University university;

    public CourseMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllCourses());
        commands.put("b", () -> addCourse());
        commands.put("c", () -> removeCourse());
        commands.put("d", () -> exit.set(true));
        while (!exit.get()) {
            showCourseMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showCourseMenu() {
        System.out.println();
        System.out.println("*** COURSE MENU ***");
        System.out.println("a. Print all courses");
        System.out.println("b. Add course");
        System.out.println("c. Remove course");
        System.out.println("d. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addCourse() {
        System.out.println("\n*** ADD NEW COURSE ***");
        System.out.println("Enter new course name >>> ");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.println("Enter new course description >>> ");
        String description = scanner.nextLine();
        Course course = new Course(name, description);
        university.addCourse(course);
        System.out.println("Course " + course.getName() + " added");
    }

    private void removeCourse() {
        printAllCourses();
        System.out.print("\nEnter course name to remove >>> ");
        String courseName = getCourseName();
        Course course = university.getCourses().stream().filter(course1 -> (course1.getName()).equals(courseName)).findFirst().get();
        university.removeCourse(course);
        System.out.println("Course " + course.getName() + " removed");
    }

    private String getCourseName() {
        return scanner.next();
    }

    public void printAllCourses() {
        System.out.println("\nUniversity courses: ");
        for (int i = 0; i < university.getCourses().size(); i++) {
            Course course = university.getCourses().get(i);
            System.out.println((i + 1) + " " + course.getName());
        }
    }
}
