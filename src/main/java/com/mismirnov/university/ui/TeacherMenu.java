package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.Course;
import com.mismirnov.university.domain.Teacher;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class TeacherMenu {

    private Scanner scanner;
    private University university;

    public TeacherMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllTeachers(false));
        commands.put("b", () -> addTeacher());
        commands.put("c", () -> removeTeacher());
        commands.put("d", () -> addTeacherCourse());
        commands.put("e", () -> removeTeacherCourse());
        commands.put("g", () -> exit.set(true));
        while (!exit.get()) {
            showTeacherMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showTeacherMenu() {
        System.out.println();
        System.out.println("*** TEACHER MENU ***");
        System.out.println("a. Print all teachers");
        System.out.println("b. Add teacher");
        System.out.println("c. Remove teacher");
        System.out.println("d. Add teacher course");
        System.out.println("e. Remove teacher course");
        System.out.println("f. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addTeacher() {
        System.out.print("Enter first name >>> ");
        String firstName = scanner.next();
        System.out.println("Enter last name >>> ");
        String lastName = scanner.next();
        System.out.println("Enter middleName name >>> ");
        String middleName = scanner.next();
        Teacher teacher = new Teacher(firstName, lastName, middleName);
        university.addTeacher(teacher);
        System.out.println("Teacher " + teacher.getFirstName() + " " + teacher.getLastName() + " added.");
    }

    private void removeTeacher() {
        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teacher1 -> (teacher1.getFirstName() + teacher1.getMiddleName() + teacher1.getLastName()).equals(firstName + middleName +lastName)).findFirst().get();
        university.removeTeacher(teacher);
        System.out.println("Teacher " + teacher.getFirstName() + " " + teacher.getLastName() + " removed.");
    }

    private void addTeacherCourse() {
        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teacher1 -> (teacher1.getFirstName() + teacher1.getMiddleName() + teacher1.getLastName()).equals(firstName + middleName +lastName)).findFirst().get();
        printTeacherCourses(teacher);
        System.out.println("\nUniversity courses: ");
        printAllCourses();
        System.out.print("\nEnter course number to add to teacher courses >>> ");
        String courseName = getName();
        Course course = university.getCourses().stream().filter(course1 -> (course1.getName()).equals(courseName)).findFirst().get();
        university.addTeacherCourse(teacher, course);
        System.out.println("Course " + course.getName() + " added to " +
                teacher.getFirstName() + " " + teacher.getLastName() + " courses");
    }

    private void removeTeacherCourse() {
        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teacher1 -> (teacher1.getFirstName() + teacher1.getMiddleName() +teacher1.getLastName()).equals(firstName + middleName +lastName)).findFirst().get();
        printTeacherCourses(teacher);
        System.out.print("\nEnter course name to remove from teacher courses >>> ");
        String courseName = getName();
        Course course = teacher.getCourses().stream().filter(course1 -> (course1.getName()).equals(courseName)).findFirst().get();
        university.removeTeacherCourse(teacher, course);
        System.out.println("Course " + course.getName() + " removed from " +
                teacher.getFirstName() + " " + teacher.getLastName() + " courses.");

    }

    public void printAllTeachers(boolean printCourses) {
        System.out.println("\nUniversity teachers: ");
        for (int i = 0; i < university.getTeachers().size(); i++) {
            Teacher teacher = university.getTeachers().get(i);
            System.out.println((i + 1) + ". " + teacher.getFirstName() + " " + teacher.getLastName() + " " + teacher.getMiddleName());
            if (printCourses) {
                for (Course course : teacher.getCourses()) {
                    System.out.println("   " + course.getName());
                }
            }
        }
    }

    public void printTeacherCourses(Teacher teacher) {
        System.out.println("\n" + teacher.getFirstName() + " " + teacher.getLastName() + " courses: ");
        for (int i = 0; i < teacher.getCourses().size(); i++) {
            Course course = teacher.getCourses().get(i);
            System.out.println((i + 1) + " " + course.getName());
        }
    }

    public void printAllCourses() {
        System.out.println("\nUniversity courses: ");
        for (int i = 0; i < university.getCourses().size(); i++) {
            Course course = university.getCourses().get(i);
            System.out.println((i + 1) + " " + course.getName());
        }
    }

    private String getName() {
        return scanner.next();
    }
}
