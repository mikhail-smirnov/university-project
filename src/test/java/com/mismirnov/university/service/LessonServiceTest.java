package com.mismirnov.university.service;

import com.mismirnov.university.domain.*;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


public class LessonServiceTest {

    private LessonService service;

    @Before
    public void setUp() {
        service = new LessonService();
    }

    @Test
    public void givenNewLesson_whenAddLesson_thenAddLesson() {
        Teacher teacher = new Teacher("vvvv", "fff", "gg");
        Course course = new Course("fff", "fff");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("09:00:00"), LocalTime.parse("10:30:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);

        service.add(lesson);

        assertThat(service.getAll(), hasItem(lesson));
    }

    @Test
    public void givenExistingLesson_whenAddLesson_thenNoChanges() {
        Teacher teacher = new Teacher("abc", "abc", "fd");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);
        service.add(lesson);
        int expectedSize = service.getAll().size();

        service.add(new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom()));

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenNewLesson_whenRemoveLesson_thenNoChanges() {
        int expectedSize = service.getAll().size();
        Teacher teacher = new Teacher("abc", "abc", "ffds");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);

        service.remove(lesson);

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenExitingLesson_whenRemoveLesson_thenRemoveLesson() {
        Teacher teacher = new Teacher("abc", "abc", "ffddd");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);
        service.add(lesson);

        service.remove(lesson);

        assertThat(service.getAll(), not(hasItem(lesson)));
    }

    @Test
    public void givenUpdatedLesson_whenUpdateLesson_thenUpdateLesson() {
        Teacher teacher = new Teacher("abc", "abc", "fddd");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);
        service.add(lesson);
        Lesson updatedLesson = new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom());
        updatedLesson.setCourse(new Course("cdef", "gftr"));
        updatedLesson.setTeacher(new Teacher("fdfd", "rere", "dss"));

        service.update(lesson, updatedLesson);

        assertThat(service.getAll().get(0), equalTo(updatedLesson));
    }

    @Test
    public void givenGroup_whenAddGroupToLesson_thenGroupAdded() {
        Teacher teacher = new Teacher("abc", "abc", "ffff");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);
        service.add(lesson);
        Group newGroup = new Group("abc");

        service.addGroup(lesson, newGroup);

        assertThat(service.getAll().get(0).getGroups(), hasItem(newGroup));
    }

    @Test
    public void givenGroup_whenRemoveGroupFromLesson_thenGroupRemoved() {
        Teacher teacher = new Teacher("abc", "abc", "fff");
        Course course = new Course("abcd", "abcd");
        LocalDate date = LocalDate.parse("2019-12-20");
        ClassRoom classRoom = new ClassRoom(1234);
        LessonTime time = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups = new ArrayList<>();
        Group group = new Group("abcabc");
        groups.add(group);
        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);
        service.add(lesson);

        service.removeGroup(lesson, group);

        assertThat(service.getAll().get(0).getGroups(), not(hasItem(group)));
    }

    @Test
    public void givenStudentAndDates_whenGetStudentSchedule_thenGetStudentSchedule() {
        Student student = new Student("fdre", "dsew", "ffd");

        Teacher teacher1 = new Teacher("abc", "abc", "fds");
        Course course1 = new Course("abcd", "abcd");
        LocalDate date1 = LocalDate.parse("2019-12-20");
        ClassRoom classRoom1 = new ClassRoom(123);
        LessonTime time1 = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups1 = new ArrayList<>();
        List<Student> students1 = new ArrayList<>();
        students1.add(student);
        Group group1 = new Group("HBF", students1);
        groups1.add(group1);
        Lesson lesson1 = new Lesson(teacher1, groups1, course1, date1, time1, classRoom1);
        Teacher teacher2 = new Teacher("dfds", "sfdw", "ffd");
        Course course2 = new Course("frgr", "bghy");
        LocalDate date2 = LocalDate.parse("2019-12-21");
        ClassRoom classRoom2 = new ClassRoom(1234);
        LessonTime time2 = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups2 = new ArrayList<>();
        List<Student> students2 = new ArrayList<>();
        students2.add(student);
        Group group2 = new Group("DSRE", students1);
        groups2.add(group2);
        Lesson lesson2 = new Lesson(teacher2, groups2, course2, date2, time2, classRoom2);

        Teacher teacher3 = new Teacher("dfds", "sfdw", "fdd");
        Course course3 = new Course("frgr", "bghy");
        LocalDate date3 = LocalDate.parse("2019-12-22");
        ClassRoom classRoom3 = new ClassRoom(1234);
        LessonTime time3 = new LessonTime(LocalTime.parse("08:00:00"), LocalTime.parse("09:45:00"));
        List<Group> groups3 = new ArrayList<>();
        List<Student> students3 = new ArrayList<>();
        students3.add(student);
        Group group3 = new Group("DSRE", students1);
        groups3.add(group3);
        Lesson lesson3 = new Lesson(teacher3, groups3, course3, date3, time3, classRoom3);

        List<Lesson> expected = new ArrayList<>();
        expected.add(lesson1);
        expected.add(lesson2);

        service.add(lesson1);
        service.add(lesson2);
        service.add(lesson3);
        List<Lesson> actual = service.getStudentSchedule(student, LocalDate.parse("2019-12-20"), LocalDate.parse("2019-12-21"));

        assertThat(actual, equalTo(expected));
    }
}
