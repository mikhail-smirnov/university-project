package com.mismirnov.university.service;

import com.mismirnov.university.domain.ClassRoom;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ClassRoomServiceTest {

    private ClassRoomService service;

    @Before
    public void setUp() {
        service = new ClassRoomService();
    }

    @Test
    public void givenExistingClassRoom_whenAdd_thenNoChanges() {
        ClassRoom classRoom = new ClassRoom(1);
        service.add(classRoom);
        int expectedSize = service.getAll().size();

        service.add(new ClassRoom(1));

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenExistingClassRoom_whenRemove_thenRemove() {
        ClassRoom audience = new ClassRoom(1);
        service.add(audience);

        service.remove(audience);

        assertThat(service.getAll(), not(hasItem(audience)));
    }

    @Test
    public void givenNewClassRoom_whenAdd_thenAdded() {
        ClassRoom classRoom = new ClassRoom(1);
        service.add(classRoom);

        assertThat(service.getAll(), hasItem(classRoom));
    }

    @Test
    public void givenNewClassRoom_whenRemove_thenNoChanges() {
        int expectedSize = service.getAll().size();
        ClassRoom classRoom = new ClassRoom(1);

        service.remove(classRoom);

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenNull_whenRemove_thenNoChanges() {
        int expectedSize = service.getAll().size();

        service.remove(null);

        assertThat(service.getAll(), hasSize(expectedSize));
    }
}