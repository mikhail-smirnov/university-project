package com.mismirnov.university.service;

import com.mismirnov.university.domain.Group;
import com.mismirnov.university.domain.Student;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;


import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;

public class GroupServiceTest {

    private GroupService service;

    @Before
    public void setUp() {
        service = new GroupService();
    }

    @Test
    public void givenNewGroup_whenAddGroup_thenAdded() {
        Group group = new Group("f3");
        service.add(group);

        assertThat(service.getAll(), hasItem(group));
    }

    @Test
    public void givenExistingGroup_whenAddGroup_thenNoChanges() {
        Group group = new Group("f3");
        service.add(group);
        int expectedSize = service.getAll().size();

        service.add(new Group("f3"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }

    @Test
    public void givenExistingGroup_whenRemoveGroup_thenRemoved() {
        Group group = new Group("afr");
        service.add(group);
        int sizeBeforeRemove = service.getAll().size();

        service.remove(group);

        assertThat(service.getAll(), hasSize(sizeBeforeRemove - 1));
        assertThat(service.getAll(), not(hasItem(group)));
    }

    @Test
    public void givenNewGroup_whenRemoveGroup_thenNoChanges() {
        int sizeBeforeRemove = service.getAll().size();

        service.remove(new Group("abc"));

        assertThat(service.getAll(), hasSize(sizeBeforeRemove));
    }


    @Test
    public void givenNewStudent_whenAddToGroup_thenAddedToGroup() {
        Group group = new Group("f3");
        service.add(group);
        Student student = new Student("ff", "ff", "ff");
        service.addStudent(group, student);

        assertThat(service.getAll().get(0).getStudents(), hasItem(student));
    }

    @Test
    public void givenExistingStudent_whenAddToGroup_thenNoChanges() {
        Group group = new Group("gp");
        service.add(group);
        Student student = new Student("frank", "cde", "gfee");
        service.addStudent(group, student);
        int expectedSize = service.getAll().get(0).getStudents().size();

        service.addStudent(group, new Student("frank", "cde", "gfee"));

        assertThat(service.getAll().get(0).getStudents(), hasSize(expectedSize));
    }

    @Test
    public void givenNewStudent_whenRemoveFromGroup_thenNoChanges() {
        Group group = new Group("f4");
        service.add(group);
        Student student = new Student("ddd", "fff", "gggg");
        service.addStudent(group, student);
        int expectedSize = service.getAll().get(0).getStudents().size();

        service.removeStudent(group, new Student("ced", "adc", "ffff"));

        assertThat(service.getAll().get(0).getStudents(), hasSize(expectedSize));
    }

    @Test
    public void givenExistingStudent_whenRemoveFromGroup_thenRemoveFromGroup() {
        Group group = new Group("hh");
        service.add(group);
        Student student = new Student("abc", "cde", "fff");
        service.addStudent(group, student);
        service.removeStudent(group, student);

        assertThat(service.getAll().get(0).getStudents(), not(hasItem(student)));
    }


    @Test
    public void givenExistingStudent_whenGetStudentGroup_thenGetStudentGroup() {
        Group group = new Group("gg");
        service.add(group);
        Student student = new Student("abc", "cde", "ffff");
        service.addStudent(group, student);

        assertThat(service.getStudentGroup(student), equalTo(group));
    }

    @Test
    public void givenNewStudent_whenGetStudentGroup_thenGetNull() {
        Group group = new Group("f4");
        service.add(group);
        Student student = new Student("fffd", "fffd", "ffff");
        service.addStudent(group, student);

        assertThat(service.getStudentGroup(new Student("ffd", "fre", "ggf")), is(IsNull.nullValue()));
    }


    @Test
    public void givenExistingStudentAndGroups_whenChangeStudentGroup_thenChangeStudentGroup() {
        Group group1 = new Group("abc");
        service.add(group1);
        Student student = new Student("abc", "cde", "ggg");
        service.addStudent(group1, student);
        Group group2 = new Group("cde");
        service.add(group2);
        service.changeStudentGroup(student, group1, group2);

        assertThat(group1.getStudents(), not(hasItem(student)));
        assertThat(group2.getStudents(), hasItem(student));
    }

    @Test
    public void givenNewStudentAndExistingGroups_whenChangeStudentGroup_thenNoChanges() {
        Group group1 = new Group("abc");
        Group group2 = new Group("cde");
        service.add(group1);
        service.add(group2);
        int group1ExpectedSize = group1.getStudents().size();
        int group2ExpectedSize = group2.getStudents().size();

        service.changeStudentGroup(new Student("abc", "abc", "ffff"), group1, group2);

        assertThat(group1.getStudents(), hasSize(group1ExpectedSize));
        assertThat(group2.getStudents(), hasSize(group2ExpectedSize));
    }
}
