package com.mismirnov.university.service;

import com.mismirnov.university.domain.LessonTime;

import java.util.ArrayList;
import java.util.List;

public class LessonTimeService {

    private List<LessonTime> times = new ArrayList<>();

    public List<LessonTime> getAll() {
        return times;
    }

    public void add(LessonTime time) {
        if (!times.contains(time)) {
            times.add(time);
        }
    }

    public void remove(LessonTime time) {
        if (!times.contains(time)) {
            times.remove(time);
        }
    }

    public List<LessonTime> getTimes() {
        return times;
    }
}
