package com.mismirnov.university.service;

import com.mismirnov.university.domain.Group;
import com.mismirnov.university.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class GroupService {

    private List<Group> groups = new ArrayList<>();

    public List<Group> getAll() {
        return groups;
    }

    public void add(Group group) {
        if (!groups.contains(group)) {
            groups.add(group);
        }
    }

    public void remove(Group group) {
        if (groups.contains(group)) {
            groups.remove(group);
        }
    }

    public void addStudent(Group group, Student student) {
        int groupIndex = groups.indexOf(group);
        if (groupIndex != -1) {
            int studentIndex = groups.get(groupIndex).getStudents().indexOf(student);
            if (studentIndex == -1) {
                groups.get(groupIndex).getStudents().add(student);
            }
        }
    }

    public void removeStudent(Group group, Student student) {
        int groupIndex = groups.indexOf(group);
        if (groupIndex != -1) {
            int studentIndex = groups.get(groupIndex).getStudents().indexOf(student);
            if (studentIndex != -1) {
                groups.get(groupIndex).getStudents().remove(studentIndex);
            }
        }
    }

    public void removeStudent(Student student) {
        int groupIndex = -1;
        int studentIndex = -1;
        for (Group group : groups) {
            for (Student studentInGroup : group.getStudents()) {
                if (studentInGroup.equals(student)) {
                    groupIndex = groups.indexOf(group);
                    studentIndex = group.getStudents().indexOf(student);
                }
            }
        }
        if (groupIndex != -1 && studentIndex != -1) {
            groups.get(groupIndex).getStudents().remove(studentIndex);
        }
    }

    public Group getStudentGroup(Student student) {
        for (Group group : groups) {
            List<Student> students = group.getStudents();
            if (students.contains(student)) {
                return group;
            }
        }
        return null;
    }

    public void changeStudentGroup(Student student, Group oldGroup, Group newGroup) {
        int oldGroupIndex = groups.indexOf(oldGroup);
        int newGroupIndex = groups.indexOf(newGroup);
        if (oldGroupIndex != -1 && newGroupIndex != -1) {
            int oldGroupStudentIndex = groups.get(oldGroupIndex).getStudents().indexOf(student);
            if (oldGroupStudentIndex != -1) {
                removeStudent(groups.get(oldGroupStudentIndex), student);
                addStudent(groups.get(newGroupIndex), student);
            }
        }
    }
}
