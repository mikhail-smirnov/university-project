package com.mismirnov.university.service;

import com.mismirnov.university.domain.Course;
import org.junit.*;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
;


public class CourseServiceTest {


    private CourseService service;

    @Before
    public void setUp() {
        service = new CourseService();
    }

    @Test
    public void givenNewCourse_whedAdd_thenAdded() {
        Course course = new Course("abc", "123");
        service.add(course);

        assertThat(service.getAll(), hasItem(course));
    }

    @Test
    public void givenExistingCourse_whenAdd_thenNoChanges() {
        Course course = new Course("abc", "123");
        service.add(course);
        int expectedSize = service.getAll().size();

        service.add(new Course("abc", "123"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }


    @Test
    public void givenExistingCourse_whenRemove_thenRemoved() {
        Course course = new Course("abc", "123");
        service.add(course);
        int sizeBeforeRemove = service.getAll().size();

        service.remove(course);

        assertThat(service.getAll(), hasSize(sizeBeforeRemove - 1));
        assertThat(service.getAll(), not(hasItem(course)));
    }

    @Test
    public void givenNewCourse_whenRemove_thenNoChanges() {
        int expectedSize = service.getAll().size();

        service.remove(new Course("abc", "123"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }

}
