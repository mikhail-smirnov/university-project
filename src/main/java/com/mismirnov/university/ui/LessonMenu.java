package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.*;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class LessonMenu {

    private Scanner scanner;
    private University university;

    public LessonMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllLessons());
        commands.put("b", () -> addLesson());
        commands.put("c", () -> removeLesson());
        commands.put("d", () -> changeLessonMenu(selectLesson()));
        commands.put("e", () -> exit.set(true));
        while (!exit.get()) {
            printLessonMenu();
            commands.get(scanner.next()).run();
        }
    }

    private void changeLessonMenu(Lesson lesson) {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> changeLessonClassRoom(lesson));
        commands.put("b", () -> changeLessonTeacher(lesson));
        commands.put("c", () -> changeLessonTime(lesson));
        commands.put("d", () -> changeLessonDate(lesson));
        commands.put("e", () -> addLessonGroup(lesson));
        commands.put("f", () -> removeLessonGroup(lesson));
        commands.put("g", () -> exit.set(true));
        while (!exit.get()) {
            printLessonMenu();
            commands.get(scanner.next()).run();
        }
    }

    private void printLessonMenu() {
        System.out.println();
        System.out.println("*** LESSON MENU ***");
        System.out.println("a. Print all lessons");
        System.out.println("b. Add lesson");
        System.out.println("c. Remove lesson");
        System.out.println("d. Change lesson");
        System.out.println("e. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void printChangeLessonMenu(Lesson lesson) {
        System.out.println();
        System.out.println("** Lesson " + lesson.getCourse().getName() + " by " +
                lesson.getTeacher().getFirstName() + " " +
                lesson.getTeacher().getLastName() + " **");
        System.out.println("a. Change classroom");
        System.out.println("b. Change teacher");
        System.out.println("c. Change time");
        System.out.println("d. Change date");
        System.out.println("e. Add group");
        System.out.println("f. Remove group");
        System.out.println("g. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addLesson() {
        System.out.println("\n*** ADD NEW LESSON ***");

        printAllCourses();
        System.out.print("\nEnter course name >>> ");
        String courseName = getName();
        Course course = university.getCourses().stream().filter(course1 -> (course1.getName()).equals(courseName)).findFirst().get();
        printAllClassRooms();
        System.out.print("\nEnter audience number >>> ");
        int classRoomIndex = getListNumber(university.getClassRooms().size());
        ClassRoom classRoom = university.getClassRooms().get(classRoomIndex);

        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter  middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teacher1 -> (teacher1.getFirstName() + teacher1.getMiddleName() + teacher1.getLastName()).equals(firstName + middleName +lastName)).findFirst().get();

        LocalDate date = getDate();

        printAllLessonTimes();
        System.out.print("\nEnter time number >>> ");
        int timeIndex = getListNumber(university.getLessonTimes().size());
        LessonTime time = university.getLessonTimes().get(timeIndex);

        printAllGroups(false);
        System.out.println("\nEnter group number >>> ");
        String groupName = getName();
        Group group = university.getGroups().stream().filter(group1 -> (group1.getName()).equals(groupName)).findFirst().get();
        List<Group> groups = new ArrayList<>();
        groups.add(group);

        Lesson lesson = new Lesson(teacher, groups, course, date, time, classRoom);

        university.addLesson(lesson);

        System.out.println("New lesson added:");
        printLesson(lesson);
    }

    private void removeLesson() {
        printAllLessons();
        System.out.println("Enter number of lesson to remove");
        int lessonIndex = getListNumber(university.getLessons().size());
        Lesson lesson = university.getLessons().get(lessonIndex);
        university.removeLesson(lesson);
        System.out.println("Lesson removed from schedule");
    }

    private void changeLessonClassRoom(Lesson lesson) {
        System.out.println("Selected lesson audience: " + lesson.getClassRoom().getRoomNumber());
        printAllClassRooms();
        System.out.print("\nEnter new classroom number >>> ");
        int classRoomIndex = getListNumber(university.getClassRooms().size());
        ClassRoom classRoom = university.getClassRooms().get(classRoomIndex);
        Lesson updatedLesson = new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom());
        updatedLesson.setClassRoom(classRoom);
        university.updateLesson(lesson, updatedLesson);
        System.out.println("Lesson audience changed to " + classRoom.getRoomNumber());
    }

    private void changeLessonTeacher(Lesson lesson) {
        System.out.println("\nSelected lesson teacher: " +
                lesson.getTeacher().getFirstName() + " " + lesson.getTeacher().getLastName());
        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter  middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teacher1 -> (teacher1.getFirstName() + teacher1.getMiddleName() + teacher1.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        Lesson updatedLesson = new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom());
        updatedLesson.setTeacher(teacher);
        university.updateLesson(lesson, updatedLesson);
        System.out.println("Lesson teacher changed");
    }

    private void changeLessonTime(Lesson lesson) {
        System.out.println("\nSelected lesson time: " +
                lesson.getLessonTime().getStartTime() + " - " + lesson.getLessonTime().getEndTime());
        printAllLessonTimes();
        System.out.print("\nEnter new time number >>> ");
        int timeIndex = getListNumber(university.getLessonTimes().size());
        LessonTime time = university.getLessonTimes().get(timeIndex);
        Lesson updatedLesson = new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom());
        updatedLesson.setLessonTime(time);
        university.updateLesson(lesson, updatedLesson);
        System.out.println("Lesson time changed");
    }

    private void changeLessonDate(Lesson lesson) {
        System.out.println("\nSelected lesson date: " + lesson.getDate());
        LocalDate date = getDate();
        Lesson updatedLesson = new Lesson(lesson.getTeacher(), lesson.getGroups(), lesson.getCourse(),lesson.getDate(),lesson.getLessonTime(), lesson.getClassRoom());
        updatedLesson.setDate(date);
        university.updateLesson(lesson, updatedLesson);
        System.out.println("Lesson date changed");
    }

    private void addLessonGroup(Lesson lesson) {
        System.out.print("\nSelected lesson groups: ");
        for (Group group : lesson.getGroups()) {
            System.out.print(group.getName() + " ");
        }
        printAllGroups(false);
        System.out.print("Enter new group name >>> ");
        String groupName = getName();
        Group group = university.getGroups().stream().filter(group1 -> (group1.getName()).equals(groupName)).findFirst().get();
        university.addLessonGroup(lesson, group);
        System.out.println("Group " + group.getName() + " added to lesson");
    }

    private void removeLessonGroup(Lesson lesson) {
        System.out.println("\nSelected lesson groups: ");
        for (int i = 0; i < lesson.getGroups().size(); i++) {
            Group group = lesson.getGroups().get(i);
            System.out.println((i + 1) + " " + group.getName());
        }
        System.out.print("Enter group name to remove >>> ");
        String groupName = getName();
        Group group = university.getGroups().stream().filter(group1 -> (group1.getName()).equals(groupName)).findFirst().get();
        university.removeLessonGroup(lesson, group);
        System.out.println("Group " + group.getName() + " removed from lesson");
    }

    private Lesson selectLesson() {
        printAllLessons();
        System.out.print("\nEnter lesson number >>> ");
        int lessonIndex = getListNumber(university.getLessons().size());
        return university.getLessons().get(lessonIndex);
    }

    private String getName() {
        String name = null;
        while (name == null) {
            try {
                name = scanner.next();
            } catch (Exception e) {
                scanner.next();
            }
        }
        return name;
    }

    private LocalDate getDate() {
        LocalDate date = null;
        boolean done = false;
        while (!done) {
            System.out.print("Enter date in format YYYY-mm-dd >>> ");
            String dateString = scanner.next();
            try {
                date = LocalDate.parse(dateString);
                done = true;
            } catch (Exception e) {
                System.out.println("Error! Date should be in format YYYY-mm-dd");
            }
        }
        return date;
    }

    private int getListNumber(int maxNumber) {
        int number = 0;
        while (number < 1 || number > maxNumber) {
            try {
                number = scanner.nextInt();
                if (number < 1 || number > maxNumber) {
                    System.out.print("Error! Please enter number from 1 to " + maxNumber + " >>> ");
                }
            } catch (Exception e) {
                scanner.next();
                System.out.print("Error! Please enter number from 1 to " + maxNumber + " >>> ");
            }
        }
        return number - 1;
    }

    public void printAllTeachers(boolean printCourses) {
        System.out.println("\nUniversity teachers: ");
        for (int i = 0; i < university.getTeachers().size(); i++) {
            Teacher teacher = university.getTeachers().get(i);
            System.out.println((i + 1) + ". " + teacher.getFirstName() + " " + teacher.getLastName() + " " + teacher.getMiddleName());
            if (printCourses) {
                for (Course course : teacher.getCourses()) {
                    System.out.println("   " + course.getName());
                }
            }
        }
    }

    public void printAllCourses() {
        System.out.println("\nUniversity courses: ");
        for (int i = 0; i < university.getCourses().size(); i++) {
            Course course = university.getCourses().get(i);
            System.out.println((i + 1) + " " + course.getName());
        }
    }

    public void printAllLessons() {
        for (int i = 0; i < university.getLessons().size(); i++) {
            Lesson lesson = university.getLessons().get(i);
            System.out.println();
            System.out.println((i + 1) + ". lesson");
            System.out.println("Course: " + lesson.getCourse().getName());
            System.out.println("Teacher: " + lesson.getTeacher().getFirstName() + " " +
                    lesson.getTeacher().getLastName());
            System.out.println("Date: " + lesson.getDate());
            System.out.println("Time: " + lesson.getLessonTime().getStartTime() + " - " + lesson.getLessonTime().getEndTime());
            System.out.println("Classroom: " + lesson.getClassRoom().getRoomNumber());
            System.out.print("Groups: ");
            for (Group group : lesson.getGroups()) {
                System.out.print(group.getName() + " ");
            }
            System.out.println();
        }
    }

    public void printLesson(Lesson lesson) {
        System.out.println("Course: " + lesson.getCourse().getName());
        System.out.println("Teacher: " + lesson.getTeacher().getFirstName() + " " +
                lesson.getTeacher().getLastName());
        System.out.println("Date: " + lesson.getDate());
        System.out.println("Time: " + lesson.getLessonTime().getStartTime() + " - " + lesson.getLessonTime().getEndTime());
        System.out.println("Classroom: " + lesson.getClassRoom().getRoomNumber());
        System.out.print("Groups: ");
        for (Group group : lesson.getGroups()) {
            System.out.print(group.getName() + " ");
        }
        System.out.println();
    }

    public void printAllClassRooms() {
        university.getClassRooms().stream().forEach(classRoom -> System.out.println(classRoom.getRoomNumber()));
    }

    public void printAllLessonTimes() {
        university.getLessonTimes().stream().forEach(lessonTime -> System.out.println(lessonTime.getStartTime() + "-" + lessonTime.getEndTime()));
    }

    public void printAllGroups(boolean printStudents) {
        System.out.println();
        for (int i = 0; i < university.getGroups().size(); i++) {
            Group group = university.getGroups().get(i);
            System.out.println((i + 1) + ". Group : " + group.getName());
            if (printStudents) {
                for (Student student : group.getStudents()) {
                    System.out.println(" " + student.getFirstName() + " " + student.getLastName());
                }
            }
        }
    }
}



