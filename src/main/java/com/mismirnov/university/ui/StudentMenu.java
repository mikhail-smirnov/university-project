package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.Student;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class StudentMenu {
    private Scanner scanner;
    private University university;

    public StudentMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllStudents());
        commands.put("b", () -> addStudent());
        commands.put("c", () -> removeStudent());
        commands.put("d", () -> exit.set(true));
        while (!exit.get()) {
            showStudentMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showStudentMenu() {
        System.out.println();
        System.out.println("*** STUDENT MENU ***");
        System.out.println("a. Print all students");
        System.out.println("b. Add student");
        System.out.println("c. Remove student");
        System.out.println("d. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addStudent() {
        System.out.print("Enter student first name >>> ");
        String firstName = scanner.next();
        System.out.print("Enter student last name >>> ");
        String lastName = scanner.next();
        System.out.print("Enter student middle name >>> ");
        String middleName = scanner.next();
        Student student = new Student(firstName, lastName, middleName);
        university.addStudent(student);
        System.out.println("Student " + firstName + " " + lastName + " added.");
    }

    private void removeStudent() {
        printAllStudents();
        System.out.print("Enter student firstname >>> ");
        String firstName = getStudentName();
        System.out.print("Enter student middle name >>> ");
        String middleName = getStudentName();
        System.out.print("Enter student last name >>> ");
        String lastName = getStudentName();
        Student studentToRemove = university.getStudents().stream().filter(student -> (student.getFirstName() + student.getMiddleName() + student.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        university.removeStudent(studentToRemove);
        System.out.println("Student " + studentToRemove.getFirstName() + " " +
                studentToRemove.getLastName() + " removed.");
    }

    private String getStudentName() {
        return scanner.next();
    }


    public void printAllStudents() {
        university.getStudents().stream().forEach(student -> System.out.println(student.getFirstName() + " " + student.getMiddleName() + " " + student.getLastName()));
    }
}
