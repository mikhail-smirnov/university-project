package com.mismirnov.university.service;

import com.mismirnov.university.domain.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseService {

    private List<Course> courses = new ArrayList<>();

    public List<Course> getAll() {
        return courses;
    }

    public void add(Course course) {
        if (!courses.contains(course)) {
            courses.add(course);
        }
    }

    public void remove(Course course) {
        if (courses.contains(course)) {
            courses.remove(course);
        }
    }
}

