package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.ClassRoom;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClassRoomMenu {
    private Scanner scanner;
    private University university;


    public ClassRoomMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllStudents());
        commands.put("b", () -> addClassRoom());
        commands.put("c", () -> removeClassRoom());
        commands.put("d", () -> exit.set(true));
        while (!exit.get()) {
            showClassRoomMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showClassRoomMenu() {
        System.out.println();
        System.out.println("*** AUDIENCE MENU ***");
        System.out.println("a. Print all audiences");
        System.out.println("b. Add audience");
        System.out.println("c. Remove audience");
        System.out.println("d. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addClassRoom() {
        System.out.print("\nEnter new classroom number >>> ");
        int roomNumber = getClassRoomNumber();
        ClassRoom classRoom = new ClassRoom(roomNumber);
        university.addClassRoom(classRoom);
        System.out.println("Audience " + classRoom.getRoomNumber() + " added");
    }

    private void removeClassRoom() {
        printAllClassRooms();
        System.out.print("\nEnter classroom to remove >>> ");
        int classRoomNumber = getClassRoomNumber();
        ClassRoom classRoom = university.getClassRooms().get(classRoomNumber);
        university.removeClassRoom(classRoom);
        System.out.println("Audience " + classRoom.getRoomNumber() + " removede");
    }


    public void printAllStudents() {
        university.getStudents().stream().forEach(student -> System.out.println(student.getFirstName() + " " + student.getMiddleName() + " " + student.getLastName()));
    }

    public void printAllClassRooms() {
        university.getClassRooms().stream().forEach(classRoom -> System.out.println(classRoom.getRoomNumber()));
    }

    private int getClassRoomNumber() {
        int number = 0;
        while (number < 1) {
            try {
                number = scanner.nextInt();
                if (number < 1) {
                    System.out.print("Error! Please enter number mroe than 1 >>> ");
                }
            } catch (Exception e) {
                scanner.next();
                System.out.print("Error! Please enter number mroe than 1 >>> ");
            }
        }
        return number - 1;
    }
}
