package com.mismirnov.university;

import com.mismirnov.university.service.*;
import com.mismirnov.university.ui.UserInterface;

public class Main {

    public static void main(String[] args) {

        ClassRoomService classRoomService = new ClassRoomService();
        CourseService courseService = new CourseService();
        GroupService groupService = new GroupService();
        LessonService lessonService = new LessonService();
        LessonTimeService lessonTimeService = new LessonTimeService();
        StudentService studentService = new StudentService();
        TeacherService teacherService = new TeacherService();

        University university = new University(studentService, teacherService, groupService, classRoomService, lessonService, courseService, lessonTimeService);
        TestDataGenerator data = new TestDataGenerator();
        data.fillUniversityData(university);
        UserInterface userInterface = new UserInterface(university);
        userInterface.run();
    }
}