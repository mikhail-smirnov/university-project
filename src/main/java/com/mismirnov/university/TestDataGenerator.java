package com.mismirnov.university;

import com.github.javafaker.Faker;
import com.mismirnov.university.domain.*;
import com.mismirnov.university.service.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class TestDataGenerator {

    private StudentService studentService;
    private TeacherService teacherService;
    private GroupService groupService;
    private ClassRoomService classRoomService;
    private LessonService lessonService;
    private CourseService courseService;
    private LessonTimeService timeService;


    public void fillUniversityData(University university) {
        generateData();
        university.setStudentService(studentService);
        university.setTeacherService(teacherService);
        university.setGroupService(groupService);
        university.setClassRoomService(classRoomService);
        university.setLessonService(lessonService);
        university.setCourseService(courseService);
        university.setTimeService(timeService);
    }

    private void generateData() {
        studentService = getStudentService();
        groupService = getGroupService();
        courseService = getCourseService();
        teacherService = getTeacherService();
        classRoomService = getClassRoomService();
        timeService = getLessonTimeService();
        lessonService = getLessonService();
    }

    private StudentService getStudentService() {
        StudentService studentService = new StudentService();
        Faker faker = new Faker();
        for (int i = 0; i < 20; i++) {
            studentService.add(new Student(faker.firstName(), faker.lastName(), faker.lastName()));
        }
        return studentService;
    }

    private GroupService getGroupService() {
        GroupService groupService = new GroupService();
        groupService.add(new Group("PI-1a"));
        groupService.add(new Group("PI-2b"));
        for (int i = 0; i < 10; i++) {
            groupService.addStudent(groupService.getAll().get(0), studentService.getAll().get(i));
        }
        for (int i = 10; i < 20; i++) {
            groupService.addStudent(groupService.getAll().get(1), studentService.getAll().get(i));
        }
        return groupService;
    }

    private CourseService getCourseService() {
        CourseService courseService = new CourseService();
        courseService.add(new Course("Economics", "Description"));
        courseService.add(new Course("Programming", "Description"));
        courseService.add(new Course("Accounting", "Description"));
        return courseService;
    }

    private ClassRoomService getClassRoomService() {
        ClassRoomService audienceService = new ClassRoomService();
        audienceService.add(new ClassRoom(1));
        audienceService.add(new ClassRoom(2));
        audienceService.add(new ClassRoom(3));
        audienceService.add(new ClassRoom(4));
        audienceService.add(new ClassRoom(5));
        return audienceService;
    }

    private LessonTimeService getLessonTimeService() {
        LessonTimeService timeService = new LessonTimeService();
        timeService.add(new LessonTime(LocalTime.parse("09:00:00"), LocalTime.parse("10:30:00")));
        timeService.add(new LessonTime(LocalTime.parse("10:15:00"), LocalTime.parse("11:45:00")));
        timeService.add(new LessonTime(LocalTime.parse("12:30:00"), LocalTime.parse("14:00:00")));
        timeService.add(new LessonTime(LocalTime.parse("14:15:00"), LocalTime.parse("15:45:00")));
        timeService.add(new LessonTime(LocalTime.parse("16:00:00"), LocalTime.parse("17:30:00")));
        return timeService;
    }

    private TeacherService getTeacherService() {
        TeacherService teacherService = new TeacherService();
        teacherService.add(new Teacher("Victor", "Diavovich", "Bbb", new ArrayList<Course>() {{
            add(courseService.getAll().get(0));
            add(courseService.getAll().get(1));
        }}));
        teacherService.add(new Teacher("Georg", "Berg", "Hett", new ArrayList<Course>() {{
            add(courseService.getAll().get(0));
        }}));
        return teacherService;
    }

    private LessonService getLessonService() {
        LessonService lessonService = new LessonService();
        lessonService.add(getLesson(0, 0, "2019-09-02", 0, 0, new ArrayList<>(Arrays.asList("PI-1a", "PI-2b"))));
        lessonService.add(getLesson(1, 1, "2019-09-03", 1, 1, new ArrayList<>(Arrays.asList("PI-1a", "PI-2b"))));
        lessonService.add(getLesson(2, 0, "2019-09-03", 0, 2, new ArrayList<>(Arrays.asList("PI-2b"))));
        lessonService.add(getLesson(0, 1, "2019-09-03", 1, 3, new ArrayList<>(Arrays.asList("PI-1a"))));
        return lessonService;
    }

    private Lesson getLesson(int courseNumber, int teacherNumber, String lessonDate, int lessonTimeSection, int classRoomNumber, List<String> groups) {
        Course course = courseService.getAll().get(courseNumber);
        Teacher teacher = teacherService.getAll().get(teacherNumber);
        LocalDate date = LocalDate.parse(lessonDate);
        LessonTime time = timeService.getAll().get(lessonTimeSection);
        ClassRoom classRoom = classRoomService.getAll().get(classRoomNumber);
        List<Group> groupsList = new ArrayList<>();
        for (String groupName : groups) {
            groupsList.add(groupService.getAll().stream().filter(group -> group.getName() == groupName).collect(Collectors.toList()).get(0));
        }

        return new Lesson(teacher, groupsList, course, date, time, classRoom);
    }

}