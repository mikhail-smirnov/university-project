package com.mismirnov.university.service;

import com.mismirnov.university.domain.LessonTime;
import org.junit.Before;
import org.junit.Test;


import java.time.LocalTime;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class LessonTimeServiceTest {

    private LessonTimeService service;

    @Before
    public void setUp() {
        service = new LessonTimeService();
    }

    @Test
    public void givenNewLessonTime_whenAddLessonTime_thenLessonTimeAdded() {
        LessonTime lessonTime = new LessonTime(LocalTime.parse("18:00:00"), LocalTime.parse("20:00:00"));
        service.add(lessonTime);

        assertThat(service.getAll(), hasItem(lessonTime));
    }

    @Test
    public void givenExistingLessonTime_whenAddLessonTime_thenNoChanges() {
        LessonTime lessonTime = new LessonTime(LocalTime.parse("18:00:00"), LocalTime.parse("20:00:00"));
        service.add(lessonTime);
        int expectedSize = service.getAll().size();

        service.add(new LessonTime(LocalTime.parse("18:00:00"), LocalTime.parse("20:00:00")));

        assertThat(service.getAll(), hasSize(expectedSize));
    }
}
