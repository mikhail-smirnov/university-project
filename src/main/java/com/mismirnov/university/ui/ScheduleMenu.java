package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.Course;
import com.mismirnov.university.domain.Lesson;
import com.mismirnov.university.domain.Student;
import com.mismirnov.university.domain.Teacher;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScheduleMenu {


    private Scanner scanner;
    private University university;

    public ScheduleMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> getStudentSchedule());
        commands.put("b", () -> getTeacherSchedule());
        commands.put("c", () -> exit.set(true));
        while (!exit.get()) {
            showScheduleMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showScheduleMenu() {
        System.out.println();
        System.out.println("*** SCHEDULE MENU ***");
        System.out.println("a. Print student schedule");
        System.out.println("b. Print teacher schedule");
        System.out.println("c. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void getStudentSchedule() {
        System.out.println();
        printAllStudents();
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Student student = university.getStudents().stream().filter(stud -> (stud.getFirstName() + stud.getMiddleName() + stud.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        System.out.println("Student " + student.getFirstName() + " " + student.getLastName() + " selected");
        System.out.println("Enter schedule start day");
        LocalDate startDate = getDate();
        System.out.println("Enter schedule end day");
        LocalDate endDate = getDate();
        List<Lesson> studentSchedule = university.getStudentSchedule(student, startDate, endDate);
        printSchedule(studentSchedule);
    }

    private void getTeacherSchedule() {
        System.out.println();
        printAllTeachers(false);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Teacher teacher = university.getTeachers().stream().filter(teach -> (teach.getFirstName() + teach.getMiddleName() + teach.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        System.out.println("Teacher " + teacher.getFirstName() + " " + teacher.getLastName() + " selected");
        System.out.println("Enter schedule start day");
        LocalDate startDate = getDate();
        System.out.println("Enter schedule end day");
        LocalDate endDate = getDate();
        List<Lesson> teacherSchedule = university.getTeacherSchedule(teacher, startDate, endDate);
        printSchedule(teacherSchedule);
    }

    private LocalDate getDate() {
        LocalDate date = null;
        boolean done = false;
        while (!done) {
            System.out.print("Enter date in format YYYY-mm-dd >>> ");
            String dateString = scanner.next();
            try {
                date = LocalDate.parse(dateString);
                done = true;
            } catch (Exception e) {
                System.out.println("Error! Date should be in format YYYY-mm-dd");
            }
        }
        return date;
    }


    public void printAllStudents() {
        university.getStudents().stream().forEach(student -> System.out.println(student.getFirstName() + " " + student.getMiddleName() + " " + student.getLastName()));
    }

    public void printAllTeachers(boolean printCourses) {
        System.out.println("\nUniversity teachers: ");
        for (int i = 0; i < university.getTeachers().size(); i++) {
            Teacher teacher = university.getTeachers().get(i);
            System.out.println((i + 1) + ". " + teacher.getFirstName() + " " + teacher.getLastName() + " " + teacher.getMiddleName());
            if (printCourses) {
                for (Course course : teacher.getCourses()) {
                    System.out.println("   " + course.getName());
                }
            }
        }
    }

    public void printSchedule(List<Lesson> schedule) {
        for (int i = 0; i < schedule.size(); i++) {
            Lesson lesson = schedule.get(i);
            System.out.println();
            System.out.println((i + 1) + ". lesson " + lesson.getCourse().getName() + ", teacher " + lesson.getTeacher().getLastName());
            System.out.println("Date: " + lesson.getDate());
            System.out.println("Time: " + lesson.getLessonTime().getStartTime() + " - " + lesson.getLessonTime().getEndTime());
            System.out.println("Class room: " + lesson.getClassRoom().getRoomNumber());
        }
    }

    private String getName() {
        return scanner.next();

    }

}
