package com.mismirnov.university.service;

import com.mismirnov.university.domain.Course;
import com.mismirnov.university.domain.Teacher;

import java.util.ArrayList;
import java.util.List;

public class TeacherService {

    private List<Teacher> teachers = new ArrayList<>();

    public List<Teacher> getAll() {
        return teachers;
    }

    public void add(Teacher teacher) {
        if (!teachers.contains(teacher)) {
            teachers.add(teacher);
        }
    }

    public void remove(Teacher teacher) {
        if (teachers.contains(teacher)) {
            teachers.remove(teacher);
        }
    }

    public void addCourse(Teacher teacher, Course course) {
        int teacherIndex = teachers.indexOf(teacher);
        if (teacherIndex != -1) {
            int courseIndex = teachers.get(teacherIndex).getCourses().indexOf(course);
            if (courseIndex == -1) {
                teachers.get(teacherIndex).getCourses().add(course);
            }
        }
    }

    public void removeCourse(Teacher teacher, Course course) {
        int teacherIndex = teachers.indexOf(teacher);
        if (teacherIndex != -1) {
            int courseIndex = teachers.get(teacherIndex).getCourses().indexOf(course);
            if (courseIndex != -1) {
                teachers.get(teacherIndex).getCourses().remove(course);
            }
        }
    }
}
