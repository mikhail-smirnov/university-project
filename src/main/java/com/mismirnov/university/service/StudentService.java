package com.mismirnov.university.service;

import com.mismirnov.university.domain.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentService {

    private List<Student> students = new ArrayList<>();

    public List<Student> getAll() {
        return students;
    }

    public void add(Student student) {
        if (!students.contains(student)) {
            students.add(student);
        }
    }

    public void remove(Student student) {
        if (students.contains(student)) {
            students.remove(student);
        }
    }
}
