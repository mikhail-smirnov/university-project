package com.mismirnov.university.service;

import com.mismirnov.university.domain.ClassRoom;

import java.util.ArrayList;
import java.util.List;

public class ClassRoomService {

    private List<ClassRoom> classRooms = new ArrayList<>();

    public List<ClassRoom> getAll() {
        return classRooms;
    }

    public void add(ClassRoom classRoom) {
        if (!classRooms.contains(classRoom)) {
            classRooms.add(classRoom);
        }
    }

    public void remove(ClassRoom classRoom) {
        if (classRooms.contains(classRoom)) {
            classRooms.remove(classRoom);
        }
    }
}
