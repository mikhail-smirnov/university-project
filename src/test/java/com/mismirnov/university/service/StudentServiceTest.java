package com.mismirnov.university.service;

import com.mismirnov.university.domain.Student;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class StudentServiceTest {

    private StudentService service;

    @Before
    public void setUp() {
        service = new StudentService();
    }

    @Test
    public void givenNewStudent_whenAddStudent_thenStudentAdded() {
        Student student = new Student("abc", "cde", "f");
        assertThat(service.getAll(), not(hasItem(student)));
        service.add(student);

        assertThat(service.getAll(), hasItem(student));
    }

    @Test
    public void givenExistingStudent_whenAddStudent_thenNoChanges() {
        Student student = new Student("abc", "cde", "f");
        service.add(student);
        int expectedSize = service.getAll().size();

        service.add(new Student("abc", "cde", "f"));

        assertThat(service.getAll(), hasSize(expectedSize));
    }


    @Test
    public void givenExistingStudent_whenRemoveStudent_thenStudentRemoved() {
        Student student = new Student("abc", "cde", "ff");
        assertThat(service.getAll(), not(hasItem(student)));
        service.add(student);

        service.remove(student);

        assertThat(service.getAll(), not(hasItem(student)));
    }

    @Test
    public void givenNewStudent_whenRemoveStudent_thenNoChanges() {
        int expectedSize = service.getAll().size();
        Student student = new Student("abc", "bce", "fff");

        service.remove(student);

        assertThat(service.getAll(), hasSize(expectedSize));
    }

}
