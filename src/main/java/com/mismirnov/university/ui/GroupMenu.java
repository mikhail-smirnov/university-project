package com.mismirnov.university.ui;

import com.mismirnov.university.University;
import com.mismirnov.university.domain.Group;
import com.mismirnov.university.domain.Student;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class GroupMenu {

    private Scanner scanner;
    private University university;

    public GroupMenu(Scanner scanner, University university) {
        this.scanner = scanner;
        this.university = university;
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> printAllGroups(false));
        commands.put("b", () -> addGroup());
        commands.put("c", () -> removeGroup());
        commands.put("d", () -> changeStudentGroup());
        commands.put("e", () -> addStudentToGroup());
        commands.put("f", () -> removeStudentFromGroup());
        commands.put("g", () -> exit.set(true));
        while (!exit.get()) {
            showGroupMenu();
            commands.get(scanner.next()).run();
        }

    }

    private void showGroupMenu() {
        System.out.println();
        System.out.println("*** GROUP MENU ***");
        System.out.println("a. Print all groups");
        System.out.println("b. Add group");
        System.out.println("c. Remove group");
        System.out.println("d. Change student group");
        System.out.println("e. Add student to group");
        System.out.println("f. Remove student from group");
        System.out.println("g. Back to Main Menu");
        System.out.print("Enter menu-letter >>> ");
    }

    private void addGroup() {
        System.out.print("Enter new group name >>> ");
        String name = scanner.next();
        Group group = new Group(name);
        university.addGroup(group);
        System.out.println("Group " + group.getName() + " added");
    }

    private void removeGroup() {
        printAllGroups(false);
        System.out.print("Enter group name >>> ");
        String groupName = getName();
        Group groupToRemove = university.getGroups().stream().filter(group -> (group.getName()).equals(groupName)).findFirst().get();
        university.removeGroup(groupToRemove);
        System.out.println("Group " + groupToRemove.getName() + " removed.");
    }

    private void changeStudentGroup() {
        printAllStudents();
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter  middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Student student = university.getStudents().stream().filter(student1 -> (student1.getFirstName() + student1.getMiddleName() + student1.getLastName() ).equals(firstName + middleName + lastName)).findFirst().get();
        Group oldGroup = university.getStudentGroup(student);
        if (oldGroup != null) {
            System.out.println("Student " + student.getFirstName() + " " +
                    student.getLastName() + " are now in group " + oldGroup.getName() + ".");
        } else {
            System.out.println("Student " + student.getFirstName() + " " +
                    student.getLastName() + " has no group.");
        }
        printAllGroups(true);
        System.out.print("Enter new group name >>> ");
        String newGroupName = getName();
        Group newGroup = university.getGroups().stream().filter(group -> (group.getName()).equals(newGroupName)).findFirst().get();
        if (oldGroup != null) {
            university.changeStudentGroup(student, oldGroup, newGroup);
            System.out.println("Student group changed.");
        } else {
            university.addStudentToGroup(student, newGroup);
            System.out.println("Student added to group.");
        }
    }

    private void addStudentToGroup() {
        printAllStudents();
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter  middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Student student = university.getStudents().stream().filter(student1 -> (student1.getFirstName() + student1.getMiddleName() + student1.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        printAllGroups(true);
        System.out.print("Enter new group number >>> ");
        String groupName = getName();
        Group group = university.getGroups().stream().filter(group1 -> (group1.getName()).equals(groupName)).findFirst().get();

        System.out.println("Student " + student.getFirstName() + " " +
                student.getLastName() + " added to group " + group.getName());
    }

    private void removeStudentFromGroup() {
        printAllGroups(true);
        System.out.print("Enter group name >>> ");
        String groupName = getName();
        Group group = university.getGroups().stream().filter(group1 -> (group1.getName()).equals(groupName)).findFirst().get();
        printGroupStudents(group);
        System.out.print("Enter firstname >>> ");
        String firstName = getName();
        System.out.print("Enter  middle name >>> ");
        String middleName = getName();
        System.out.print("Enter last name >>> ");
        String lastName = getName();
        Student student = group.getStudents().stream().filter(student1 -> (student1.getFirstName() + student1.getMiddleName() + student1.getLastName()).equals(firstName + middleName + lastName)).findFirst().get();
        university.removeStudent(student);
        System.out.println("Student " + student.getFirstName() + " " +
                student.getLastName() + " removed from group " + group.getName());

    }

    private void printAllStudents() {
        System.out.println("\nSTUDENTS: ");
        for (int i = 0; i < university.getStudents().size(); i++) {
            Student student = university.getStudents().get(i);
            System.out.println((i + 1) + ". " + student.getFirstName() + " " + student.getLastName());
        }
    }

    private String getName() {
        return scanner.next();
    }

    public void printAllGroups(boolean printStudents) {
        System.out.println();
        for (int i = 0; i < university.getGroups().size(); i++) {
            Group group = university.getGroups().get(i);
            System.out.println((i + 1) + ". Group : " + group.getName());
            if (printStudents) {
                for (Student student : group.getStudents()) {
                    System.out.println(" " + student.getFirstName() + " " + student.getLastName());
                }
            }
        }
    }

    public void printGroupStudents(Group group) {
        System.out.println("GROUP " + group.getName() + " STUDENTS:");
        for (int i = 0; i < group.getStudents().size(); i++) {
            Student student = group.getStudents().get(i);
            System.out.println((i + 1) + " " + student.getFirstName() + " " + student.getLastName());
        }
    }
}
