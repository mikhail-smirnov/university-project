package com.mismirnov.university.ui;

import com.mismirnov.university.University;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class UserInterface {

    private Scanner scanner;
    private StudentMenu studentMenu;
    private GroupMenu groupMenu;
    private TeacherMenu teacherMenu;
    private LessonMenu lessonMenu;
    private ClassRoomMenu classRoomMenu;
    private CourseMenu courseMenu;
    private ScheduleMenu scheduleMenu;

    public UserInterface(University university) {
        this.scanner = new Scanner(System.in);
        this.studentMenu = new StudentMenu(scanner, university);
        this.groupMenu = new GroupMenu(scanner, university);
        this.teacherMenu = new TeacherMenu(scanner, university);
        this.lessonMenu = new LessonMenu(scanner, university);
        this.classRoomMenu = new ClassRoomMenu(scanner, university);
        this.courseMenu = new CourseMenu(scanner, university);
        this.scheduleMenu = new ScheduleMenu(scanner, university);
    }

    public void run() {
        final AtomicBoolean exit = new AtomicBoolean(false);
        Map<String, Runnable> commands = new HashMap<>();
        commands.put("a", () -> studentMenu.run());
        commands.put("b", () -> groupMenu.run());
        commands.put("c", () -> teacherMenu.run());
        commands.put("d", () -> lessonMenu.run());
        commands.put("e", () -> classRoomMenu.run());
        commands.put("f", () -> courseMenu.run());
        commands.put("g", () -> scheduleMenu.run());
        commands.put("h", () -> exit.set(true));
        while (!exit.get()) {
            printMainMenu();
            commands.get(scanner.next()).run();
        }
        scanner.close();
    }

    private void printMainMenu() {
        System.out.println();
        System.out.println("*** MAIN MENU ***");
        System.out.println("a. Students");
        System.out.println("b. Groups");
        System.out.println("c. Teachers");
        System.out.println("d. Lessons");
        System.out.println("e. Audiences");
        System.out.println("f. Courses");
        System.out.println("g. Schedules");
        System.out.println("h. Exit program");
        System.out.print("Enter menu-letter >>> ");
    }
}