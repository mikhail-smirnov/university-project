package com.mismirnov.university.domain;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Lesson implements Cloneable {

    private Teacher teacher;
    private List<Group> groups;
    private Course course;
    private LocalDate date;
    private LessonTime lessonTime;
    private ClassRoom classRoom;

    public Lesson() {
    }

    public Lesson(Teacher teacher, List<Group> groups, Course course, LocalDate date, LessonTime lessonTime, ClassRoom classRoom) {
        this.teacher = teacher;
        this.groups = groups;
        this.course = course;
        this.date = date;
        this.lessonTime = lessonTime;
        this.classRoom = classRoom;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LessonTime getLessonTime() {
        return lessonTime;
    }

    public void setLessonTime(LessonTime lessonTime) {
        this.lessonTime = lessonTime;
    }

    public ClassRoom getClassRoom() {
        return classRoom;
    }

    public void setClassRoom(ClassRoom classRoom) {
        this.classRoom = classRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return Objects.equals(teacher, lesson.teacher) &&
                Objects.equals(groups, lesson.groups) &&
                Objects.equals(course, lesson.course) &&
                Objects.equals(date, lesson.date) &&
                Objects.equals(lessonTime, lesson.lessonTime) &&
                Objects.equals(classRoom, lesson.classRoom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacher, groups, course, date, lessonTime, classRoom);
    }
}
